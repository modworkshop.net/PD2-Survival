ObjectivesManager = ObjectivesManager or class()
ObjectivesManager.PATH = "gamedata/objectives"
ObjectivesManager.FILE_EXTENSION = "objective"
ObjectivesManager.FULL_PATH = ObjectivesManager.PATH .. "." .. ObjectivesManager.FILE_EXTENSION
ObjectivesManager.REMINDER_INTERVAL = 240
function ObjectivesManager:activate_objective(id, load_data, data)
	if not id or not self._objectives[id] then
		Application:stack_dump_error("Bad id to activate objective, " .. tostring(id) .. ".")
		return
	end
	local objective = self._objectives[id]
	for _, sub_objective in pairs(objective.sub_objectives) do
		sub_objective.completed = false
	end
	objective.current_amount = load_data and load_data.current_amount or data and data.amount and 0 or objective.current_amount
	objective.amount = load_data and load_data.amount or data and data.amount or objective.amount
	if Global.mission_manager.saved_job_values.NextWave then
			Global.mission_manager.saved_job_values.CurrentWave = Global.mission_manager.saved_job_values.CurrentWave + 1
			objective.text =  "Wave: " .. Global.mission_manager.saved_job_values.CurrentWave
	end
	local activate_params = {
		id = id,
		text = objective.text,
		sub_objectives = objective.sub_objectives,
		amount = objective.amount,
		current_amount = objective.current_amount,
		amount_text = objective.amount_text
	}
	self._delayed_presentation = nil
	if data and data.delay_presentation then
		self._delayed_presentation = {t = 1, activate_params = activate_params}
	else
		managers.hud:activate_objective(activate_params)
	end
	if not load_data then
		local title_message = data and data.title_message or managers.localization:text("mission_objective_activated")
		local text = objective.text
		if self._delayed_presentation then
			self._delayed_presentation.mid_text_params = {
				text = text,
				title = title_message,
				time = 4,
				icon = nil,
				event = "stinger_objectivecomplete"
			}
		else
			managers.hud:present_mid_text({
				text = text,
				title = title_message,
				time = 4,
				icon = nil,
				event = "stinger_objectivecomplete"
			})
		end
	end
	self._active_objectives[id] = objective
	self._remind_objectives[id] = {
		next_t = Application:time() + self.REMINDER_INTERVAL
	}
end