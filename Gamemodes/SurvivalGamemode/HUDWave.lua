HUDWave = HUDWave or class()

function HUDWave:init(hud)
    self._hud_panel = hud.panel

    self._panel = self._hud_panel:panel({
        name = "wave_panel",
        visible = false,
        w = 300,
        h = 100
    })
    self._panel:set_lefttop(self._hud_panel:child("objectives_panel"):leftbottom())

    self._wave_text = self._hud_panel:text({
        name = "wave_text",
        layer = 1,
        text = "Wave: ",
        font_size = tweak_data.hud.active_objective_title_font_size,
		font = tweak_data.hud.medium_font_noshadow,
    })
    managers.hud:make_fine_text(self._wave_text)
    self._wave_text:set_center_y(self._panel:h() / 2)

    self._enemy_text = self._hud_panel:text({
        name = "enemy_text",
        layer = 1,
        text = "0/0",
        font_size = tweak_data.hud.active_objective_title_font_size,
		font = tweak_data.hud.medium_font_noshadow,
    })
    managers.hud:make_fine_text(self._enemy_text)
    self._enemy_text:set_right(self._panel:w())
    self._enemy_text:set_center_y(self._panel:h() / 2)
end

function HUDWave:update_wave(data)
    self._panel:set_visible(not data.hide)
    if data.active then
        self:set_wave_text("Wave: " .. data.wave)

        self:set_enemy_text(data.enemies .. " enemies left")
    else
        self:set_enemy_text(nil)
        self:set_wave_text(string.format("Wave %s upcoming, x seconds remaining", data.wave + 1))
    end
end

function HUDWave:set_wave_text(text)
    self._wave_text:set_text(text)
    managers.hud:make_fine_text(self._wave_text)
    self._wave_text:set_x(0)
end

function HUDWave:set_enemy_text(text)
    self._enemy_text:set_visible(not not text)
    text = text or ""
    self._enemy_text:set_text(text)
    managers.hud:make_fine_text(self._enemy_text)
    self._enemy_text:set_right(self._panel:w())
end
