core:import("CoreMissionScriptElement")
ElementWave = ElementWave or class(CoreMissionScriptElement.MissionScriptElement)
ElementWave._current_wave = 0
ElementWave._wave_active = false
ElementWave._base_spawn_amount = 20
ElementWave._wave_spawn_multiplier = 1.2

function ElementWave:init(...)
    self.super.init(self, ...)
end

function ElementWave:on_script_activated()
    self._mission_script:add_save_state_cb(self._id)
end

function ElementWave:client_on_executed(...)
    self:on_executed(...)
end

function ElementWave:on_executed(instigator)
    if not self._values.enabled then
        return
    end

    if not self._wave_active then
        self:start_wave()
    else

        local to_spawn = self:get_mission_element(self._values._spawn_counter):counter_value()
        local alive = self:get_mission_element(self._values._alive_counter):counter_value()

        if to_spawn == 0 and alive == 0 then
            self:end_wave()
        end

    end

	local spawn_group = self:get_mission_element(self._values._spawn_group)
	local alive_counter = self:get_mission_element(self._values._alive_counter)._values.counter_target

	if alive_counter > 40 then
		spawn_group._values.enabled = false
	elseif alive_counter < 20 then
		spawn_group._values.enabled = true
	end

    self:update_hud()

    self.super.on_executed(self, instigator)
end

function ElementWave:start_wave()
    self._current_wave = self._current_wave + 1
    self._wave_active = true

    local amount_to_spawn = self._base_spawn_amount
    for i = 1, self._current_wave do
        amount_to_spawn = amount_to_spawn * self._wave_spawn_multiplier
    end
    amount_to_spawn = math.floor(amount_to_spawn)

    self:get_mission_element(self._values._spawn_counter):counter_operation_set(amount_to_spawn)
    --choose spawn ratio and set it to the spawn groups

	local spawn_group = self:get_mission_element(self._values._spawn_group)

	for _, id in ipairs(spawn_group._values.elements) do
		local element = self:get_mission_element(id)
		element._values.enemy = "units/payday2/characters/ene_city_swat_3/ene_city_swat_3"
	end

	if self._wave_active and not managers.groupai:state():get_hunt_mode() then
		managers.groupai:state():set_wave_mode("hunt")
	end
end

function ElementWave:end_wave()
    self._wave_active = false
    managers.groupai:state():set_wave_mode("quiet")
end

function ElementWave:get_enemy_count()
    local to_spawn = self:get_mission_element(self._values._spawn_counter):counter_value()
    local alive = self:get_mission_element(self._values._alive_counter):counter_value()

    return to_spawn + alive
end

function ElementWave:update_hud(enemies)
    managers.hud:update_wave_hud({
        wave = self._current_wave,
        enemies = enemies or self:get_enemy_count(),
        active = self._wave_active
    })
end

function ElementWave:save(data)
	data.enabled = self._values.enabled
end

function ElementWave:load(data)
	self:set_enabled(data.enabled)
end
