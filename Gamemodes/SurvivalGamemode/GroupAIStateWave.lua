GroupAIStateWave = GroupAIStateWave or class(GroupAIStateBase)
GroupAIStateWave.base_enemy_wave = 20
GroupAIStateWave.enemy_wave_multiplier = 1.2

function GroupAIStateWave:init()
    self.super.init(self)

    self._current_wave = 0
    self._enemies_to_spawn = 0
end

function GroupAIStateWave:_upd_police_activity()
	self._police_upd_task_queued = false
	if self._ai_enabled then
		self:_upd_SO()
		self:_upd_grp_SO()
		--self:_check_spawn_phalanx()
		--self:_check_phalanx_group_has_spawned()
		--self:_check_phalanx_damage_reduction_increase()
		if self._enemy_weapons_hot then
			self:_claculate_drama_value()
			--self:_upd_regroup_task()
			--self:_upd_reenforce_tasks()
            --self:_upd_recon_tasks()

            --self:_begin_new_tasks()

            self:_upd_assault_task()
			self:_upd_group_spawning()
			self:_upd_groups()
		end
	end
	self:_queue_police_upd_task()
end

function GroupAIStateWave:_begin_new_tasks()
	local all_areas = self._area_data
	local nav_manager = managers.navigation
	local all_nav_segs = nav_manager._nav_segments
	local task_data = self._task_data
	local t = self._t
	local assault_candidates = {}
	for criminal_key, criminal_data in pairs(self._char_criminals) do
		if not criminal_data.status then
			local nav_seg = criminal_data.tracker:nav_segment()
			local area = self:get_area_from_nav_seg_id(nav_seg)
			table.insert(assault_candidates, area)
		end
	end
	--[[local i = 1
	repeat
		local area = to_search_areas[i]
		local force_factor = area.factors.force
		local demand = force_factor and force_factor.force
		local nr_police = table.size(area.police.units)
		local nr_criminals = table.size(area.criminal.units)
		if reenforce_candidates and demand and demand > 0 and nr_criminals == 0 then
			local area_free = true
			for i_task, reenforce_task_data in ipairs(reenforce_data.tasks) do
				if reenforce_task_data.target_area == area then
					area_free = false
				else
				end
			end
			if area_free then
				table.insert(reenforce_candidates, area)
			end
		end
		if recon_candidates and (area.loot or area.hostages) then
			local occupied
			for group_id, group in pairs(self._groups) do
				if group.objective.target_area == area or group.objective.area == area then
					occupied = true
				else
				end
			end
			if not occupied then
				local is_area_safe = nr_criminals == 0
				if is_area_safe then
					if are_recon_candidates_safe then
						table.insert(recon_candidates, area)
					else
						are_recon_candidates_safe = true
						recon_candidates = {area}
					end
				elseif not are_recon_candidates_safe then
					table.insert(recon_candidates, area)
				end
			end
		end
		if assault_candidates then
			for criminal_key, _ in pairs(area.criminal.units) do
				if not self._criminals[criminal_key].status and not self._criminals[criminal_key].is_deployable then
					table.insert(assault_candidates, area)
				else
				end
			end
		end
		if nr_criminals == 0 then
			for neighbour_area_id, neighbour_area in pairs(area.neighbours) do
				if not found_areas[neighbour_area_id] then
					table.insert(to_search_areas, neighbour_area)
					found_areas[neighbour_area_id] = true
				end
			end
		end
		i = i + 1
	until i > #to_search_areas]]--
	if self._start_wave_assault and assault_candidates and #assault_candidates > 0 then
		self:_begin_assault_task(assault_candidates)
		recon_candidates = nil
        self._start_wave_assault = false
	end
end

function GroupAIStateWave:set_assault_mode(enabled)
	self.super.set_assault_mode(self, enabled)
    if self._assault_mode ~= enabled and self._assault_mode then
        self._task_data.assault.target_areas = {}
        local assault_candidates = self._task_data.assault.target_areas
        for criminal_key, criminal_data in pairs(self._char_criminals) do
            if not criminal_data.status then
                local nav_seg = criminal_data.tracker:nav_segment()
                local area = self:get_area_from_nav_seg_id(nav_seg)
                table.insert(assault_candidates, area)
            end
        end
    end
end

function GroupAIStateWave:_upd_assault_task()
	if not self._assault_mode then
		return
	end
	local t = self._t

    for criminal_key, criminal_data in pairs(self._player_criminals) do
		self:criminal_spotted(criminal_data.unit)
		for group_id, group in pairs(self._groups) do
			if group.objective.charge then
				for u_key, u_data in pairs(group.units) do
					u_data.unit:brain():clbk_group_member_attention_identified(nil, criminal_key)
				end
			end
		end
	end

		if not next(self._spawning_groups) then
			local spawn_group, spawn_group_type = self:_find_spawn_group_near_area(primary_target_area, tweak_data.group_ai.besiege.assault.groups, nil, nil, nil)
			if spawn_group then
				local grp_objective = {
					type = "assault_area",
					area = spawn_group.area,
					coarse_path = {
						{
							spawn_group.area.pos_nav_seg,
							spawn_group.area.pos
						}
					},
					attitude = "avoid",
					pose = "crouch",
					stance = "hos"
				}
				self:_spawn_in_group(spawn_group, spawn_group_type, grp_objective, task_data)
			end
        end

	if t > task_data.use_smoke_timer then
		task_data.use_smoke = true
	end
	if self._smoke_grenade_queued and task_data.use_smoke and not self:is_smoke_grenade_active() then
		self:detonate_smoke_grenade(self._smoke_grenade_queued[1], self._smoke_grenade_queued[1], self._smoke_grenade_queued[2], self._smoke_grenade_queued[4])
		if self._smoke_grenade_queued[3] then
			self._smoke_grenade_ignore_control = true
		end
	end

	self:_assign_enemy_groups_to_assault()
end

function GroupAIStateWave:start_wave(wave)
    if self._current_wave == wave then
        log(string.format("Already started the specified wave! (%s)", wave))
        return
    end

    self._current_wave = wave
    self._enemies_to_spawn = self.base_enemy_wave
    for i = 1, wave do
        self._enemies_to_spawn = self._enemies_to_spawn * self.enemy_wave_multiplier
    end
    self._enemies_to_spawn = math.floor(self._enemies_to_spawn)
    self:set_wave_mode("hunt")
    --self._start_wave_assault = true
end

function GroupAIStateWave:set_wave_mode(flag)
	local old_wave_mode = self._wave_mode
	self._wave_mode = flag
	self._hunt_mode = nil
	if flag == "hunt" then
		self._hunt_mode = true
		self._wave_mode = "besiege"
		managers.hud:start_assault()
		self:_set_rescue_state(false)
		self:set_assault_mode(true)
		managers.trade:set_trade_countdown(false)
		--self:_end_regroup_task()
	else
        managers.hud:end_assault()
        self:set_assault_mode(false)
	end
end

Hooks:Add("GroupAIStatePostInit", "SurvivalModeAddWaveState", function(self)
    self:register_state("wave", GroupAIStateWave)
end)
