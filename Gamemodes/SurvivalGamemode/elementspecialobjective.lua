local ElementSpecialObjectiveon_executed = ElementSpecialObjective.on_executed

function ElementSpecialObjective:on_executed(instigator)
	if self._values.use_player_position then
		local player_unit = managers.player:player_unit(self._values.use_player_position)
		if not player_unit then
			player_unit = managers.player:local_player()
		end
		if player_unit then
			self._values.position = player_unit:position()
		end
	end
	
	--[[if self._values.use_instigator then
		if type_name(instigator) == "Unit" and alive(instigator) then
			local ai_group_filter = {}
			table.insert(ai_group_filter, tweak_data.character[instigator:base()._tweak_table].access)
			local create_SO_access = managers.navigation:convert_access_filter_to_string(ai_group_filter)
			log(create_SO_access)
			self._values.SO_access = create_SO_access
					--local ai_group_filter = {}
		--for _, access in ipairs(values.character_access) do
		--	table.insert(ai_group_filter, access)
		--end
		end
	end]]
	
	ElementSpecialObjectiveon_executed(self, instigator)
end

local ElementSpecialObjective_finalize_values = ElementSpecialObjective._finalize_values

function ElementSpecialObjective:_finalize_values(values)

	if values.character_access then
		local create_SO_access = managers.navigation:convert_access_filter_to_string(values.character_access)
		values.SO_access = create_SO_access
	end

	ElementSpecialObjective_finalize_values(self, values)
end
