Hooks:PostHook(HUDManager, "_setup_player_info_hud_pd2", "HUDCreateWaveHUD", function(self)
    if not self:alive(PlayerBase.PLAYER_INFO_HUD_PD2) then
		return
	end
	local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2)
    self:_create_wave_hud(hud)
end)

function HUDManager:_create_wave_hud(hud)
    hud = hud or managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2)
	self._hud_wave = HUDWave:new(hud)
end

function HUDManager:update_wave_hud(data)
    if self._hud_wave then
        self._hud_wave:update_wave(data)
    end
end

--[[function HUDManager:update_wave(current_wave)
    self:update_wave_hud({ wave = current_wave })
end

function HUDManager:update_wave_enemies(num_enemies)
    self:update_wave_hud({ enemies = num_enemies})
end]]--
