local ElementSpawnEnemyGroupon_executed = ElementSpawnEnemyGroup.on_executed

function ElementSpawnEnemyGroup:on_executed(instigator)
	if self._values.counter_id then
		local counter_element = self:get_mission_element(self._values.counter_id)
		--self._group_data.amount = element:counter_value()
		Amount_to_spawn = counter_element:counter_value()
	end
	Max_spawn_amount = 20
	self._group_data.amount = Max_spawn_amount
	if Amount_to_spawn < 20 then
		self._group_data.amount = Amount_to_spawn
	end
	Amount_to_spawn = Amount_to_spawn - self._group_data.amount
	--log(Amount_to_spawn)
	local disabled_elements = 0
	local lowest_distance, closest_spawn
	
	for i, player_id in pairs(managers.player._players) do
		local player_unit = managers.player:player_unit(i)
		for _, element_id in ipairs(self._values.elements) do
			local element = self:get_mission_element(element_id)
			local spawn_pos = element._values.position
			if not player_unit then
				local player_unit = managers.player:local_player()
			end
			if player_unit then
				local player_pos = player_unit:position()
				local distance = mvector3.distance(spawn_pos, player_pos)
				--log(distance)
				if distance > 3500 then
					element._values.enabled = false
					disabled_elements = disabled_elements + 1
					--log(disabled_elements)
				elseif distance < 3500 then
					element._values.enabled = true
				end
				if not lowest_distance or distance < lowest_distance then
					--log(distance)
					lowest_distance = distance
					closest_spawn = element
				end
			end
		end
	end
	
	if disabled_elements >= 30 then 
		closest_spawn._values.enabled = true
	end

	ElementSpawnEnemyGroupon_executed(self, instigator)
end